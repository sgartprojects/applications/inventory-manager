import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {UserDataService} from "./services/user-data/user-data.service";
import { UserComponent } from './components/user/user.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { GroupComponent } from './components/group/group.component';
import { PermissionComponent } from './components/permission/permission.component';
import {GravatarService} from "./services/gravatar/gravatar.service";
import { UserEditorComponent } from './components/user-editor/user-editor.component';
import { UserEditorFormValidationDirective } from './directives/user-editor-form-validation.directive';
import {InputTextModule} from "primeng/inputtext";
import { NavigationComponent } from './components/navigation/navigation.component';
import {MenubarModule} from "primeng/menubar";
import {ButtonModule} from "primeng/button";
import {SelectButtonModule} from "primeng/selectbutton";

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    GroupComponent,
    PermissionComponent,
    UserEditorComponent,
    UserEditorFormValidationDirective,
    NavigationComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    InputTextModule,
    MenubarModule,
    ButtonModule,
    SelectButtonModule
  ],
  providers: [GravatarService, UserDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
