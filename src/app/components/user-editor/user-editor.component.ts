import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../interfaces/user-interface";

@Component({
  selector: 'app-user-editor-component',
  templateUrl: './user-editor.component.html',
  styleUrls: ['./user-editor.component.css']
})
export class UserEditorComponent implements OnInit {
  @Input()
  user?: User

  @Output()
  protected onUserCreate = new EventEmitter<User>();
  @Output()
  protected onUserEdit = new EventEmitter<User>();
  @Output()
  protected onUserDelete = new EventEmitter();

  protected creationMode?: boolean;

  constructor() {
  }

  ngOnInit(): void {
    this.creationMode = !this.user;
  }
}
