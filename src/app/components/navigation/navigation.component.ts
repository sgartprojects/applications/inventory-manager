import { Component, OnInit } from '@angular/core';
import {MenuItem} from "primeng/api";
import {User} from "../../interfaces/user-interface";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  items: MenuItem[];
  user?: User;
  loggedIn: boolean;

  constructor() {
    this.loggedIn = false;
    this.items = [
      {
        label: 'test'
      },
      {
        label: this.user?.username,
        icon: 'pi pi-user',
        id: 'account-menu',
        items: [
          {
            label: 'Settings',
            icon: 'pi pi-cog'
          },
          {
            label: 'Logout',
            icon: 'pi pi-power-off',
            command: (event) => {
              this.loggedIn = !this.loggedIn;
            }
          }
        ]
      }
    ]
  }

  ngOnInit(): void {
  }

}
