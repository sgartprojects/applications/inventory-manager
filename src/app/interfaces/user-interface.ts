import {Group} from "./group-interface";

export interface User {
  pfp_url: string,
  firstName: string,
  lastName: string,
  username: string,
  email: string,
  groups: Group[]
}
